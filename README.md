# task-micro-task

タスクとマイクロタスクを理解するためのサンプルコード。

console に出力される順番と、"Ding" とともに出力される値に注目する。

## サンプルコード

### 同期的処理のみ

- [sample-1.js](./sample/sample-1.js)

### `setTimeout(fn, 0)` あり

- [sample-2.js](./sample/sample-2.js)
- [sample-3.js](./sample/sample-3.js)
- [sample-4.js](./sample/sample-4.js)

※ sample-3.js, sample-4.js の差異はレキシカルスコープの有無

### `setTimeout(fn, 0)`, `Promise.resolve().then(fn)` あり

- [sample-5.js](./sample/sample-5.js)

※ `Promise.resolve().then(fn)` は `setTimeout(fn, 0)` よりも先に実行される

## ブラウザ

- [index-1.js](./html/index-1.js)
- [index-2.js](./html/index-2.js)

※ `addEventListener` の挙動を確認

## 参考

- [時間の話 - Google スライド](https://docs.google.com/presentation/d/e/2PACX-1vT1xgfvvkTGdSql9yQR3dBeIxhwH6eQ6ZYv4MLaKC0qASWancdZuldBVg7Axir5Fl9RMllL04PcPwdI/pub?slide=id.p)
- [Tasks, microtasks, queues and schedules - JakeArchibald.com](https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/)
