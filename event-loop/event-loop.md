# Node.js でのイベントループの仕組みとタイマーについて

## イベントループとは？

メインスレッドで実行される。

ブラウザのイベントループは html5 に基づく。

Node.js のイベントループは libuv に基づく。

libuv は、イベントループと非同期処理を行うための抽象化ライブラリである。

## タスク

**同期タスク**

- `())();`

**非同期タスク**

- `setTimeout())`
- `setInterval())`
- `setImmediate())`
- `process.nextTick())`
- `Promise.resolve().then())`

同期タスクは非同期タスクよりも早く実行される。

EventEmitter で発生するイベントはタスクとは呼ばない。

## イベントループの仕組み

- nextTickQueue
- microTaskQueue

Node.js が起動すると初期化される。

**開始時に行われること**

- タイマーのスケジュール設定
- `process.nextTick` などの実行
- 同期タスクの実行
- 非同期 API の呼び出し

イベントループは複数の task を同時に処理することはできないので、task はキューに入れられ、順次処理される。

## フェーズ

| フェーズ              | 説明                                      | メソッド                           |
| --------------------- | ----------------------------------------- | ---------------------------------- |
| timers                | Run expired timers                        | setTimeout, setInterval            |
| pending I/O callbacks | Run Completed I/O handlers                | some finished I/O work, I/O Errors |
| (idle, prepare)       |                                           |                                    |
| I/O Poll              | Wait for I/O complete                     |                                    |
| Check                 | Some checking stuff after polling fot I/O | setImmediate                       |
| Close callbacks       | Run any close handlers                    | \*.on("close", ...handlers)        |

JavaScript は、どこかのフェーズで実行される。

各フェーズは、実行するコールバックの FIFO ジョブキューを持つ。

各フェーズ後に nextTickQueue と microTaskQueue が実行される。

## nextTickQueue

`process.nextTick` を使用して登録されたコールバックを保持する。

すべての非同期タスクの中で最速。

## microTaskQueue

`Promise`オブジェクトのコールバックはここに所属する。

nextTickQueue が空になり次第実行される。

## Timers Phase

`setTimeout`, `setInterval` のタイマーの有効期限を確認し、期限が切れたもののコールバックを実行する。

複数のタイマーが存在する場合、登録した順番に実行される。

Node.js ではコールバックの実行される正確なタイミングや順序付けは保証されない。

指定された時間のできるだけ近い時間で呼び出される。

最初の Timers Phase の前（同期処理が終わってから Timer Phase が始まる前）にも Tick がある。

## Pending Callbacks Phase

## Idle, Prepare Phase

## Poll Phase

新しいソケットコネクトやファイルの読み込みなどの新しい I/O イベントを取得し、実行する。

## Check Phase

`setImmediate` のコールバック専用フェーズ。

Poll Phase で実行されていたコールバックの中に `setImmediate` が存在すれば、`setTimeout` よりも先に呼ばれることが保証される。

## Close Callbacks Phase

すべての `close` イベントのコールバックが処理される。

## 例

```JavaScript
const { readFile } = require("fs");

const timeoutScheduled = Date.now();

setTimeout(() => {
  console.log(`delay: ${Date.now() - timeoutScheduled}`)
}, 100);

readFile(__fileName, () => {
  const startCallback = Date.now();
  while (Date.now() - startCallback < 500) {
    //
  }
});
```
