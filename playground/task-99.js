const func1 = () => {
  return Promise.resolve().then(() => {
    func1();
  });
};

setTimeout(() => console.log("Timeout"));

func1();
