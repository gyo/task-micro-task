let counter = 0;

const add = () => {
  console.log(`----${counter}`);

  if (counter % 2 === 0) {
    Promise.resolve().then(() => {
      console.log(`Ding! ${counter}`);
    });
    console.log(`! ${counter}`);
  }

  counter = counter + 1;
  if (counter < 10) {
    Promise.resolve().then(add);
  }
};

let counter2 = 0;

const add2 = () => {
  console.log(`====${counter2}`);

  if (counter2 % 2 === 0) {
    Promise.resolve().then(() => {
      console.log(`Ding!! ${counter2}`);
    });
    console.log(`!! ${counter2}`);
  }

  counter2 = counter2 + 1;
  if (counter2 < 10) {
    Promise.resolve().then(add2);
  }
};

add();
add2();
