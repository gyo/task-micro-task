let counter = 0;

const add = () => {
  console.log(`----${counter}`);

  if (counter % 2 === 0) {
    setTimeout(() => console.log(`Ding! ${counter}`), 0);
    console.log(`! ${counter}`);
  }

  counter = counter + 1;
  if (counter < 10) {
    setTimeout(add, 0);
  }
};

let counter2 = 0;

const add2 = () => {
  console.log(`====${counter2}`);

  if (counter2 % 2 === 0) {
    setTimeout(() => console.log(`Ding!! ${counter2}`), 0);
    console.log(`!! ${counter2}`);
  }

  counter2 = counter2 + 1;
  if (counter2 < 10) {
    setTimeout(add2, 0);
  }
};

add();
add2();
