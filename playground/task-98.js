let x;

setTimeout(() => console.log(`setTimeout: ${x}`));

Promise.resolve()
  .then(() => (x = "Hello, world"))
  .then(() => console.log(`Promise.then: ${x}`));
