let counter = 0;

const add = () => {
  console.log(`----${counter}`);

  if (counter % 2 === 0) {
    Promise.resolve().then(() => {
      console.log(`Ding! ${counter}`);
    });
    console.log(`! ${counter}`);
  }

  counter = counter + 1;
  if (counter < 10) {
    add();
  }
};

const startButton = document.querySelector(".start");
const start2Button = document.querySelector(".start2");

startButton.addEventListener("click", () => {
  add();
  start2Button.click();
});

start2Button.addEventListener("click", () => {
  console.log("Clicked! start2");
});
